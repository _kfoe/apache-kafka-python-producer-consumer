Python Kafka Producer Consumer
=======

Requirements
----------
Apache Kafka

How to use
----------
start zookeeper
```bash
$ zookeeper-server-start /usr/local/etc/kafka/zookeeper.properties
```

start kafka
```bash
$ kafka-server-start /usr/local/etc/kafka/server.properties
```

run consumer
```bash
$ python3 consumer.py
```

run producer
```bash
$ python3 producer.py
```