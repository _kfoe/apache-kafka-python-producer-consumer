from kafka import KafkaConsumer
from json import loads

class Consumer(object):
	def __init__(self):
		self.consumer = KafkaConsumer(
		    'jokes',
		     bootstrap_servers=['localhost:9092'],
		     auto_offset_reset='earliest',
		     enable_auto_commit=True,
		     group_id='my-group',
		     value_deserializer=lambda x: loads(x.decode('utf-8'))
	     )

	def consumeJoke(self):
		for consumerRecord in self.consumer:
			print(consumerRecord.value)

if __name__ == '__main__':
	consumer = Consumer()
	consumer.consumeJoke()