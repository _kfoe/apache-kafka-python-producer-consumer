import smtplib
from email.mime.text import MIMEText
from kafka import KafkaProducer
from time import sleep
from json import dumps
import requests
from requests.structures import CaseInsensitiveDict

class Producer(object):
	def __init__(self, jokeNumbers, sleepTime = 3):
		self.running = False
		self.headers = CaseInsensitiveDict()
		self.headers["Accept"] = "application/json"

		self.url = 'https://icanhazdadjoke.com'
		self.jokeNumbers = jokeNumbers
		self.sleepTime = sleepTime
		self.producer = KafkaProducer(
			bootstrap_servers=['localhost:9092'],
			value_serializer=lambda x: dumps(x).encode('utf-8')
		)

	def produceJoke(self):
		self.running = True
		for e in range(self.jokeNumbers):
			res = requests.get(self.url, headers=self.headers)
			if res.status_code == 200:
			    data = res.json()
			    self.producer.send('jokes', value=data)
			    sleep(self.sleepTime)
		self.running = False

	def getIsRunning(self):
		return self.running

if __name__ == '__main__':
	producer = Producer(4)
	producer.produceJoke()